package views;

import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

public class addUserModal extends Dialog<ButtonType> {

    private ButtonType ok = new ButtonType("OK", ButtonData.OK_DONE);
    private ButtonType cancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
    private TextField userText;

    /**
     * Creates an add user dialog box.
     */
    public addUserModal() {
    	DialogPane dialogPane = this.getDialogPane();
    	//dialogPane.getStylesheets().add(
    	  //getClass().getResource("modal.css").toExternalForm());
    	
        setTitle("Add a user");
        setHeaderText(null);

        GridPane dPane = new GridPane();
        Text user = new Text("Username: ");
        userText = new TextField();
        userText.setPromptText("Required");

        dPane.setHgap(7D);
        dPane.setVgap(8D);

        GridPane.setConstraints(user, 0, 0);
        GridPane.setConstraints(userText, 1, 0);

        dPane.getChildren().addAll(user, userText);
        getDialogPane().getButtonTypes().addAll(ok, cancel);
        getDialogPane().setContent(dPane);
    }
    
    /**
     * Extracts username from dialog box.
     * @return username
     */
    public String getUsername() {
    	return userText.getText();
    }
    
}