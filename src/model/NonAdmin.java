package model;

import java.util.*;

/**
 * @author Toni Au
 */

public class NonAdmin extends User {
	
	private ArrayList<Album> albums;
	private static final long serialVersionUID = 1L;
	
	public NonAdmin(){}

	public NonAdmin(String u) {
		super(u);
		albums = new ArrayList<Album>();
	}
	
	public Album getAlbum(Album album){
		return albums.get(albums.indexOf(album));
	}

	public ArrayList<Album> getAlbums(){
		return albums;
	}
	
	public void addAlbum(String name){
		Album newAlbum = new Album(name);
		albums.add(newAlbum);
	}
	
}