package model;

import java.io.*;
import java.util.ArrayList;

/**
 * @author Toni Au, Sean Wu
 */

public abstract class User implements Serializable {
	
	private String username;
	private static final long serialVersionUID = 1L;
	
	public User(){}
	
	public User(String u){
		username = u.toLowerCase();
	}
	
	/**
	 * Serialization of user's data
	 */
	public void serialize(){
		try{
			FileOutputStream fileOut = new FileOutputStream("data/" + username +".ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(this);
			out.close();
			fileOut.close();
		} catch (Exception e) {
			System.out.println("Invalid Serialization");
		}
	}
	
	public String getName() {
		return username;
	}
	

}