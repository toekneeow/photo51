package model;

import java.io.*;
import java.time.LocalDateTime;
import java.util.*;

/**
 * @author Toni Au
 */

public class Photo implements Serializable {
	
	/**
	 * A photo contains the following properties
	 * - file name
	 * - caption
	 * - date photo was taken
	 * - photo tags
	 */
	private File file;
	private String caption;
	private LocalDateTime date;
	private HashMap<String, ArrayList<String>> tags;
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor for photo, takes in file name
	 * @param f
	 */
	public Photo(File f){
		file = f;
		caption = "";
		tags = new HashMap<String, ArrayList<String>>();
	}
	
	/**
	 * Get file name of photo
	 * @return photo file name
	 */
	public File getFile(){
		return file;
	}
	
	/**
	 * Get date of last modification to photo
	 * @return date of last photo mod
	 */
	public LocalDateTime getDate(){
		return date;
	}
	
	/**
	 * Get caption of photo
	 * @return photo caption
	 */
	public String getCaption(){
		return caption;
	}
	
	public void setCaption(String c){
		caption = c;
	}
	
	/**
	 * Get photo tags
	 * @return photo tags
	 */
	public HashMap<String, ArrayList<String>> getTags(){
		return tags;
	}
	
	
}