package model;

import java.io.*;
import java.time.LocalDateTime;
import java.util.*;

/**
 * @author Toni Au, Sean Wu
 */

public class Album implements Serializable {
	
	private String title;
	private ArrayList<Photo> photos;
	private LocalDateTime date;
	private static final long serialVersionUID = 1L;
	
	public Album(){}
	
	public Album(String t){
		title = t;
		photos = new ArrayList<Photo>();
	}
	
	public ArrayList<Photo> getPhotos(){
		return photos;
	}
	
	/**
	 * Adds a photo to an album (ArrayList<Photo>)
	 * @param photo
	 */
	public void addPhoto(Photo photo){
		photos.add(photo);
	}
	
	/**
	 * Removes a photo from an album (ArrayList<Photo>)
	 * @param photo
	 */
	public void removePhoto(Photo photo){
		photos.remove(photo);
	}
	
	public String getTitle(){
		return title;
	}
	
	public void setTitle(String t){
		title = t;
	}
	
	

}