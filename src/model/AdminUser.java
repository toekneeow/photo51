package model;

import java.util.*;


/**
 * @author Toni Au
 */

public class AdminUser extends User {
	
	// Complete master list of all users
	private ArrayList<User> userList;
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor for admin user
	 * @param u
	 */
	public AdminUser(String u) {
		super(u);
		userList = new ArrayList<User>();
	}
	
	/**
	 * Gets admin the complete users list
	 * @return users list
	 */
	public ArrayList<User> getUserList(){
		return userList;
	}
}