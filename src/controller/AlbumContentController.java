package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Callback;
import model.Photo;
import model.Album;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Optional;
import java.util.StringJoiner;

/**
 * 
 * @author Sean Wu
 *
 */

public class AlbumContentController extends Controller {
	private Stage stage;
	private Album album;
	private Photo currentPhoto;
	
    @FXML
    private ListView<Photo> photoListView;

    @FXML
    private TextField txtName;
    
    @FXML
    private TextField txtCaption;

    @FXML
    private TextField txtTags;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnCancel;

    @FXML
    private Button btnAdd;

    @FXML
    private Button btnCopy;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnMove;
    
    private ObservableList<Photo> photoObsList = FXCollections.observableArrayList();

    public void start(Stage mainStage){
    	stage = mainStage;
    	mainStage.setTitle(getAlbum().getTitle());
    	photoObsList = FXCollections.observableArrayList(album.getPhotos());
    	
    	photoListView.setCellFactory(new Callback<ListView<Photo>,ListCell<Photo>>(){

			@Override
			public ListCell<Photo> call(ListView<Photo> param) {
				// TODO Auto-generated method stub
				return new PhotoListCell();
			}
    		
    	});
    	
    	photoListView.setItems(photoObsList);
    	
    }
    
    private static class PhotoListCell extends ListCell<Photo>{
    	private ImageView imageView = new ImageView();
    	
    	@Override
    	public void updateItem(Photo item, boolean empty){
    		super.updateItem(item, empty);
    		
    	    if (empty || item == null) {
    	        imageView.setImage(null);

    	        setGraphic(null);
    	        setText(null);
    	    } else {
    	        imageView.setImage(album.get(item));

    	        setText(constructLabel(SHORT_PREFIX, item, SUFFIX));
    	        setGraphic(imageView);
    	    }
    	}
    }
    
    @FXML
    void btnAddAction(ActionEvent event) {
    	
    }

    @FXML
    void btnCancelAction(ActionEvent event) {
    	stage.close();
    }

    @FXML
    void btnDeleteAction(ActionEvent event, Photo photo) {
    	Alert alert = new Alert(AlertType.CONFIRMATION);
    	alert.setTitle("Delete Photo?");
    	alert.setContentText("Are you sure you want to delete this photo?");
    	ButtonType buttonTypeYes = new ButtonType("Yes");
    	ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(buttonTypeYes, buttonTypeCancel);

    	Optional<ButtonType> result = alert.showAndWait();
    	if(result.get() == ButtonType.OK){
    		album.getPhotos().remove(photo);
    	}
    	alert.close();
    }

    @FXML
    void btnMoveAction(ActionEvent event, Photo photo, Album album) {
    	this.album.addPhoto(photo);
    	album.removePhoto(photo);
    }

    @FXML
    void btnCopyAction(ActionEvent event, Photo photo, Album album) {
    	this.album.addPhoto(photo);
    }

    @FXML
    void btnSaveAction(ActionEvent event) {
    	currentPhoto.setCaption(txtName.getText());
    	currentPhoto.setTags(parseTags(txtTags.getText().toString())); //still has to be implemented
    }

    @FXML
    void photoNameClicked(MouseEvent event) {
    	currentPhoto = photoList.getSelectionModel().getSelectedItem();
    	txtName.setText(currentPhoto.getFile().getName());
    	txtCaption.setText(currentPhoto.getCaption());
    	txtTags.setText(unparseTags(currentPhoto.getTags()));
    }
    
    
    private void setAlbum(Album a){
    	album = a;
    }
    
    private Album getAlbum(){
    	return album;
    }
    
    private String unparseTags(ArrayList<String> tags){
    	StringJoiner unparsed = new StringJoiner(", ");
    	for(String tag : tags){
    		unparsed.add(tag);
    	}
    	return unparsed.toString();
    }
    
    private String parseTags(String tags){
    	String parsed = new String();
    	return parsed;
    }

}

