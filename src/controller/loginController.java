package controller;

import app.PhotoAlbumApp;
import java.io.*;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.NonAdmin;

/**
 * @author Toni Au
 */

public class loginController extends Controller{

	@FXML private TextField Username;
	@FXML private Button Login;
	@FXML private Text badUser;
	
	public void start(Stage mainStage){
	
	}
	
	/**
	 * Log in the user specified
	 * @param e
	 * @throws IOException
	 */
	public void login(ActionEvent e) throws IOException{
		String username = Username.getText().toLowerCase();
		try{
		if(username.equalsIgnoreCase("admin")){
			nextScene("/views/AdminSys.fxml");
			return;
		} 
		//search through list
		for(int i = 0; i < PhotoAlbumApp.admin.getUserList().size(); i++){
			PhotoAlbumApp.user = (NonAdmin) PhotoAlbumApp.admin.getUserList().get(i);
			if(PhotoAlbumApp.user.getName().equals(username)){
				badUser.setStyle("-fx-opacity: 0;");
				PhotoAlbumApp.loggedUser = (NonAdmin) PhotoAlbumApp.admin.getUserList().get(i);
				nextScene("/views/albumList.fxml");
				return;
			}
		}
		}catch(Exception ex){
			ex.getStackTrace();
		}

		//if not found
		badUser.setStyle("-fx-opacity: 1;");
	}
}