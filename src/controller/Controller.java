package controller;

import java.io.*;

import app.PhotoAlbumApp;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import model.User;

/**
 * @author Toni Au
 */

public abstract class Controller {
	
	public void start(Stage mainStage){
		
	}
	
	/**
	 * Change current screen
	 * @param path to desired next FXML file
	 */
	public void nextScene(String fxml){
		try{
			FXMLLoader loader = new FXMLLoader(); 
			loader.setLocation(getClass().getResource(fxml));
			Pane root;
			root = (Pane)loader.load();
			Controller login = loader.getController();
			login.start(PhotoAlbumApp.stage);
			Scene scene = new Scene(root);
			PhotoAlbumApp.stage.setScene(scene);
			PhotoAlbumApp.stage.setResizable(false);
			PhotoAlbumApp.stage.show(); 
		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	public void logout() throws IOException {
		ButtonType ok = new ButtonType("OK", ButtonData.OK_DONE);
		ButtonType cancel = new ButtonType("Cancel", ButtonData.NO);
		Dialog<ButtonType> dialog = new Dialog<ButtonType>();
		dialog.getDialogPane().getButtonTypes().add(ok);
		dialog.getDialogPane().getButtonTypes().add(cancel);
		dialog.setHeaderText("Logging Out");
		dialog.setContentText("Are you sure you want to logout?");
		dialog.showAndWait().ifPresent(response -> {
			if (response == ok) {
				for (User u : PhotoAlbumApp.admin.getUserList()) {
					u.serialize();
				}
				nextScene("/views/Login.fxml");
			}
		});
	}
}