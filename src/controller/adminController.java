package controller;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;

import app.PhotoAlbumApp;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.stage.Stage;
import model.NonAdmin;
import views.addUserModal;


/**
 * 
 * @author Toni Au
 *
 */

public class adminController extends Controller {
	
	@FXML private ListView<String> list;
	@FXML private TextField username;
	@FXML private Button createUser;
	@FXML private Button deleteUser;
	
	private ObservableList<String> obsvList;
	
	public void start(Stage mainStage){
		obsvList = FXCollections.observableArrayList();
		for (int i = 0; i < PhotoAlbumApp.admin.getUserList().size(); i++) {
			obsvList.add(i, PhotoAlbumApp.admin.getUserList().get(i).getName());
		}
		list.setItems(obsvList); 
		list.getSelectionModel().select(0);
		showUserInfo();
		list.setOnMouseClicked((e) -> showUserInfo());
	}
	
	/**
	 * Add user to Users List
	 * @param e
	 */
	public void addUser(ActionEvent e){
		addUserModal popup = new addUserModal();
		Optional<ButtonType> result = popup.showAndWait();
		String ok = ButtonType.OK.getText();
		String click = result.get().getText();
		
		if (click.equals(ok)) {
			String username = popup.getUsername().toLowerCase();
			if (username.isEmpty()) {
				Alert error = new Alert(AlertType.INFORMATION);
				error.setHeaderText("ERROR");
				error.setContentText("Username required");
				error.show();
				return;
			}
			if (username.equalsIgnoreCase("admin")) {
				Alert error = new Alert(AlertType.INFORMATION);
				error.setHeaderText("ERROR");
				error.setContentText("Cannot create a user with username \"" + username + "\".");
				error.show();
				return;
			}
			NonAdmin user = new NonAdmin(username);
			for(int i = 0; i < PhotoAlbumApp.admin.getUserList().size(); i++){
				if(username.equals(PhotoAlbumApp.admin.getUserList().get(i).getName())){
					Alert error = new Alert(AlertType.INFORMATION);
					error.setHeaderText("ERROR");
					error.setContentText("Username already in use");
					error.show();
					return;
				} else if (username.compareToIgnoreCase(PhotoAlbumApp.admin.getUserList().get(i).getName()) == 0 || username.compareToIgnoreCase(PhotoAlbumApp.admin.getUserList().get(i).getName()) < 0){
					PhotoAlbumApp.admin.getUserList().add(i, user);
					obsvList.add(i, username);
					list.getSelectionModel().select(i);
					showUserInfo();
					return;
				}
			}
			PhotoAlbumApp.admin.getUserList().add(user);
			System.out.println("Username: " + username);
			obsvList.add(username);
			list.getSelectionModel().select(obsvList.size() - 1);
			showUserInfo();
			PhotoAlbumApp.user = user;
			PhotoAlbumApp.user.serialize();
		}
	}
	
	/**
	 * Delete user from Users List
	 * @param e
	 */
	public void deleteUser(ActionEvent e) {
		int index = list.getSelectionModel().getSelectedIndex();
		if(index >= 0){
			String user = PhotoAlbumApp.admin.getUserList().get(index).getName();
			ButtonType ok = new ButtonType("OK", ButtonData.OK_DONE);
			ButtonType cancel = new ButtonType("Cancel", ButtonData.NO);
			Dialog<ButtonType> dialog = new Dialog<ButtonType>();
			dialog.getDialogPane().getButtonTypes().add(ok);
			dialog.getDialogPane().getButtonTypes().add(cancel);
			dialog.setHeaderText("Confirm.");
			dialog.setContentText("Are you sure you would like to delete the user " + user + "?");
			dialog.showAndWait().ifPresent(response -> {
				if (response == ok) {
					obsvList.remove(index);
					PhotoAlbumApp.admin.getUserList().remove(index);
					showUserInfo();
					try {
						Files.delete(Paths.get("data/" + user + ".ser"));
					} catch (Exception e1) {
						System.out.println("Invalid serialized file to be deleted.");
					}
				}
			});
		}
	}
	
	/**
	 * Display the info for the user currently highlighted
	 */
	public void showUserInfo(){
		int k = list.getSelectionModel().getSelectedIndex();
		if(k >= 0){
			username.setText(PhotoAlbumApp.admin.getUserList().get(k).getName());
		} else {
			username.setText("");
		}
	}
}