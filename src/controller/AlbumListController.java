package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.AnchorPane;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.event.ActionEvent;
import model.*;

import java.util.ArrayList;
import java.util.Optional;

import app.PhotoAlbumApp;

public class AlbumListController extends Controller {
	private Album currentAlbum;
	private NonAdmin user;
	
	private Stage stage;
	private ArrayList<Album> albums = new ArrayList<Album>();
	private ObservableList<Album> albumsObsList;
	
	@FXML
	private AnchorPane anchorPane;
	
	@FXML
	private ListView<Album> albumListView;
	
    @FXML
    private Button btnDelete;

    @FXML
    private Button btnCreate;

    @FXML
    private Button btnSearch;

    @FXML
    private Button btnRename;

    @FXML
    private Button btnSignOut;

    
    public void start(Stage mainStage){
    	stage = mainStage;
    	user = PhotoAlbumApp.loggedUser;
    	mainStage.setTitle(user.getName());
    	albumsObsList = FXCollections.observableArrayList(user.getAlbums());
    	albums = user.getAlbums();
    	
    	albumListView.setItems(albumsObsList);
    	
    }
    

    @FXML
    void btnCreateAction(ActionEvent event) {
    	TextInputDialog dialog = new TextInputDialog("");
    	dialog.setTitle("Create Album");
    	dialog.setContentText("Enter album name:");
    	
    	Optional<String> result = dialog.showAndWait();
    	if(result.isPresent()){
    		String newName = result.get();
    		user.addAlbum(newName);
    	}
    	dialog.close();
    	
    }

    @FXML
    void btnDeleteAction(ActionEvent event) {
    	Alert alert = new Alert(AlertType.CONFIRMATION, "Are you sure you want to delete this album?");
    	alert.setTitle("Delete Album");
    	ButtonType buttonTypeYes = new ButtonType("Yes");
    	ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(buttonTypeYes, buttonTypeCancel);

    	Optional<ButtonType> result = alert.showAndWait();
	    if(result.isPresent()){	
    		if(result.get() == ButtonType.OK){
	    		user.getAlbums().remove(currentAlbum);
	    	}
	    }
    	alert.close();
    }

    @FXML
    void btnRenameAction(ActionEvent event) {
    	TextInputDialog dialog = new TextInputDialog("");
    	dialog.setTitle("Rename Album");
    	dialog.setContentText("Enter new name:");
    	
    	Optional<String> result = dialog.showAndWait();
    	if(result.isPresent()){
    		currentAlbum.setTitle(result.get());
    	}
    	dialog.close();
    }

    @FXML
    void btnSearchAction(ActionEvent event) {
    	
    }

}
