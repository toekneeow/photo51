package controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import model.Album;

public class PhotoController extends Controller {

	int index = 0;
	Album album;
	
    @FXML
    private Text caption;

    @FXML
    private Text dateModified;

    @FXML
    private Text tags;

    @FXML
    private Button btnNext;

    @FXML
    private Button btnPrevious;

    @FXML
    private ImageView image;
    
    /**
     * On back click, return to previous screen
     */
    private void btnBackAction(){
    	nextScene("/views/albumContent.fxml");
    }
    
    /**
     * Controls behavior of "previous" button
     */
    private void btnPreviousAction(){
    	if(index==0)
    		index=album.getPhotos().size()-1;
    	else
    		index--;
    }

    private void btnNextAction(){
    	if(index==album.getPhotos().size()-1)
    		index =0;
    	else
    		index++;
    }
}
