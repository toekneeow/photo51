package app;

import java.io.*;
import java.util.*;
import controller.loginController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.AdminUser;
import model.Album;
import model.NonAdmin;
import model.Photo;
import model.User;

/**
 * @author Toni Au, Sean Wu
 */

public class PhotoAlbumApp extends Application{

	public static Stage stage;
	public static AdminUser admin = new AdminUser("admin");
	public static NonAdmin user;
	public static NonAdmin loggedUser;
	public static Photo photo;
	public static Album album;
	public static ArrayList<Photo> search;
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		updateUserList();
		stage = primaryStage;
		FXMLLoader loader = new FXMLLoader();   
		// Load login page
	    loader.setLocation(getClass().getResource("/views/Login.fxml"));
	    AnchorPane root = (AnchorPane)loader.load();
	    loginController login = loader.getController();
	    login.start(primaryStage);
	    // Set up the scene
	    Scene scene = new Scene(root);
	    primaryStage.setScene(scene);
	    primaryStage.setResizable(false);
	    primaryStage.show(); 
	    
	    search = new ArrayList<Photo>();
	}
	
	/**
	 * Main method to launch photo album app
	 * @param args
	 */
	public static void main(String[] args){
		launch(args);
	}
	
	/**
	 * Updates the user list from pre-existing serialized files
	 */
	public void updateUserList() {
		File x = new File("data");
		File[] directory = x.listFiles();
		if (directory != null) {
			for (File child : directory) {
				String file = child.getName();
				if (file.toLowerCase().contains(".ser")) {
					User u = deserialize(file);
					PhotoAlbumApp.admin.getUserList().add(u);
				}
		    }
		} else {
		    System.out.println("Invalid directory");
		}
	}
	
	/**
	 * Retrieves a user object from a file name
	 * @param file_name Name of the user file
	 * @return User object with its relevant data
	 */
	public User deserialize(String file) {
		User u = null;
	    try {
	    	FileInputStream fileIn = new FileInputStream("data/" + file);
	        ObjectInputStream in = new ObjectInputStream(fileIn);
	        u = (User) in.readObject();
	        in.close();
	        fileIn.close();
	    } catch(Exception e) {
	         System.out.println("Invalid deserialization, e: " + e);
	    } 
	    return u;
	}
	
}